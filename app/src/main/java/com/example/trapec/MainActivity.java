package com.example.trapec;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity{
    public EditText formulab, formulaa, formulah;
    public Button btnCalc;
    public TextView obikolkanatrapec;
    public double S, a, b, h, a1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        formulaa = findViewById(R.id.formulaa);
        formulab = findViewById(R.id.formulab);
        formulah = findViewById(R.id.formulah);
        btnCalc = findViewById(R.id.btnCalc);
        obikolkanatrapec = findViewById(R.id.obikolkanatrapec);
        btnCalc.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                try {
                    a = Double.parseDouble(formulaa.getText().toString());
                    b = Double.parseDouble(formulab.getText().toString());
                    h = Double.parseDouble(formulah.getText().toString());
                    if (a <= 0 || b <= 0) {
                        obikolkanatrapec.setText("a и b не могат да имат отрицателна стойност");
                    } else {
                        a1 = (a+b)*h;
                        S = a1/2;
                        obikolkanatrapec.setText("Резултат: " + Double.toString(S));
                    }
                } catch (Exception e) {
                    obikolkanatrapec.setText("Exception: " + e.getLocalizedMessage());
                }
            }
        });
    }
}